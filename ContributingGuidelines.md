# Contributing Guidelines #

You're very welcome to contribute to the PRP project. Please make shure to follow this guidelines.

## Git ##

All projects are managed with Git. Our workflow differs from standard workflows, so please make shure to read this guide carefuly.

You work will be done in your personal fork of the repository you want to work in. Only maintainers have access to the main repositories.

We use a rebase workflow instead of merging. The only merge that happens is the Pull Request merge after a successfull review. If you want to update your branch, please rebase on the main master or release branch.

Furthermore, please make shure to hand in only one single commit for review. So squash your work before creating the Pull Request.

## Code Style ##

For all Java projects, please use the provided formatter from this repository.
It is an Eclipse formatter. However, it should be possible to use it in other IDEs.

## Comments ##

As the PRP project is international, please write comments only in English.
