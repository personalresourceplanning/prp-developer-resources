# PRP Project #

Welcome to the PRP project! We're developing the first system, that gives you time back.

## Maintainers ##

The project is maintained by [Eric Fischer](mailto:prp@ericfischer.eu) and [Felix Schiessl](mailto:prp@felixschiessl.de) and many volunteer contributors.

## Contributors ##

Niklas Vieth
Robin Herder

# This Repository #

In this repository, you can find helpful information and tools for developing for the PRP project. You can find the Contributing Guidelines in the ContributingGuidelines.md file.

## Development Environment ##

For an easy database setup for development, use the docker-compose.yml in this repository by simply running `docker-compose up -d`. You need Docker Engine and Docker Compose installed. Maybe you need to run the command as root user.
